extern crate rug;
use std::iter::FromIterator;
use std::iter::IntoIterator;
use std::collections::VecDeque;
use pest::iterators::{Pair, Pairs};
use rug::{Float, Rational};
use crate::parser::{Rule, DATA_PREC_CLIMBER, PROGRAM_PREC_CLIMBER};

#[derive(Debug, PartialEq)]
pub struct AssociativeExpr {
    pub args : VecDeque<Expr>
}

impl AssociativeExpr {
    pub fn merge(x : AssociativeExpr, y : AssociativeExpr) -> Self {
        let mut v = x.args;
        v.extend(y.args.into_iter());
        AssociativeExpr{args : v}
    }
    pub fn new(x : Expr, y : Expr) -> Self {
        let mut v = VecDeque::with_capacity(2);
        v.push_back(x);
        v.push_back(y);
        AssociativeExpr{args : v}
    }
    pub fn append_right(&mut self, x : Expr) {self.args.push_back(x)}
    pub fn append_left(&mut self, x : Expr) {self.args.push_front(x)}
}

#[derive(Debug, PartialEq)]
pub struct AssociativeCommutativeExpr {
    pub args : Vec<Expr>
}

impl AssociativeCommutativeExpr {
    pub fn merge(x : AssociativeCommutativeExpr, y : AssociativeCommutativeExpr) -> Self {
        let (mut to_extend, extender) =  if x.args.capacity() > y.args.capacity() {
            (x.args, y.args)
        } else {
            (y.args, x.args)
        };
        to_extend.extend(extender.into_iter());
        AssociativeCommutativeExpr{args : to_extend}
    }
    pub fn new(x: Expr, y : Expr) -> Self {
        AssociativeCommutativeExpr{args : vec![x, y]}
    }
    pub fn append_right(&mut self, x : Expr) {self.args.push(x)}
    pub fn append_left(&mut self, x : Expr) {self.args.push(x)}
}

#[derive(Debug, PartialEq)]
pub struct UnaryOperatorString(Vec<Rule>);

impl UnaryOperatorString {
    pub fn constant_fold(&mut self, expr : Expr) -> Expr {
        let vec = &mut self.0;
        match expr {
            Expr::RationalExpr(mut int) => {
                while !vec.is_empty() {
                    let mut negate = false;
                    match vec[vec.len() - 1] {
                        Rule::minus => {
                            vec.pop();
                            negate = !negate
                        },
                        _ => break
                    }
                    if negate {
                        int *= Rational::from(-1) //TODO: optimize this
                    }
                }
                Expr::RationalExpr(int)
            },
            _ => expr
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Expr {
    BooleanExpr(bool),
    DecimalExpr(Float),
    RationalExpr(Rational),
    AdditionExpr(AssociativeCommutativeExpr),
    MultiplicationExpr(AssociativeCommutativeExpr),
    SubtractionExpr(Box<Expr>, Box<Expr>),
    DivisionExpr(Box<Expr>, Box<Expr>),
    JuxtapositionExpr(AssociativeExpr),
    UnaryApplicationExpr(UnaryOperatorString, Box<Expr>)
}

pub fn build_ast(pair : Pair<Rule>) -> Expr {
    match pair.as_rule() {
        Rule::expression =>
            PROGRAM_PREC_CLIMBER.climb(pair.into_inner(), build_ast, build_binary_ast),
        Rule::data_expression =>
            DATA_PREC_CLIMBER.climb(pair.into_inner(), build_ast, build_binary_ast),
        Rule::true_expression => Expr::BooleanExpr(true),
        Rule::false_expression => Expr::BooleanExpr(false),
        Rule::unary_expression => build_unary_ast(pair),
        Rule::integer => build_integer_ast(pair),
        Rule::decimal => build_decimal_ast(pair),
        Rule::percentage => build_percentage_ast(pair),
        _ => panic!(
            "Rule {:?} not yet implemented!\n\nContext:\n {:#?}\n",
            pair.as_rule(), pair)
    }
}

pub fn build_operator_string(pair : Pair<Rule>) -> UnaryOperatorString {
    UnaryOperatorString(Vec::from_iter(pair.into_inner().map(|pair| pair.as_rule())))
}

pub fn build_integer_ast(pair : Pair<Rule>) -> Expr {
    Expr::RationalExpr(pair.as_str().parse::<Rational>().unwrap())
}

pub fn build_decimal_ast(pair : Pair<Rule>) -> Expr {
    Expr::DecimalExpr(build_decimal_expr(pair))
}

pub fn build_decimal_expr(pair : Pair<Rule>) -> Float {
    let string = pair.as_str();
    let bits = f64::ceil((string.len() as f64) * f64::log2(10.0)) as u32;
    let bits = if bits < 64 {64} else {bits};
    Float::with_val(bits, Float::parse(string).unwrap())
}

pub fn build_percentage_ast(pair : Pair<Rule>) -> Expr {
    let num = pair.into_inner().next().unwrap();
    match num.as_rule() {
        Rule::integer => {
            match build_integer_ast(num) {
                Expr::RationalExpr(mut i) => {
                    i /= Rational::from(100); // TODO: make more efficient...
                    Expr::RationalExpr(i)
                },
                _ => unreachable!()
            }
        },
        Rule::decimal => Expr::DecimalExpr(build_decimal_expr(num)/100),
        _ => unreachable!()
    }
}

pub fn build_unary_ast(pair : Pair<Rule>) -> Expr {
    let mut pairs = pair.into_inner();
    match pairs.peek().unwrap().as_rule() {
        Rule::unary_operator_string => {

                let mut operator_string = build_operator_string(pairs.next().unwrap());
                let juxtaposition_expression =
                    operator_string.constant_fold(build_juxtaposition_ast(pairs));
                if operator_string.0.is_empty() {
                    juxtaposition_expression
                } else {
                    Expr::UnaryApplicationExpr(operator_string, Box::new(juxtaposition_expression))
                }
            }
        _ => build_juxtaposition_ast(pairs)
    }
}

pub fn build_juxtaposition_ast(mut pairs : Pairs<Rule>) -> Expr {
    let first = pairs.next().unwrap();
    if let Some(_) = pairs.peek() {
        let mut vec = VecDeque::new();
        vec.push_back(build_ast(first));
        vec.extend(pairs.map(|pair| build_ast(pair)));
        Expr::JuxtapositionExpr(AssociativeExpr{args : vec})
    } else {
        build_ast(first)
    }
}

fn build_addition_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::RationalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs += rhs;
            Expr::RationalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::DecimalExpr(rhs)) => {
            lhs += rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs += rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::RationalExpr(lhs), Expr::DecimalExpr(mut rhs)) => {
            rhs += lhs;
            Expr::DecimalExpr(rhs)
        },
        (Expr::AdditionExpr(lhs), Expr::AdditionExpr(rhs)) => {
            Expr::AdditionExpr(AssociativeCommutativeExpr::merge(rhs, lhs))
        }
        (Expr::AdditionExpr(mut lhs), rhs) => {
            lhs.append_right(rhs);
            Expr::AdditionExpr(lhs)
        },
        (lhs, Expr::AdditionExpr(mut rhs)) => {
            rhs.append_left(lhs);
            Expr::AdditionExpr(rhs)
        },
        _ => Expr::AdditionExpr(AssociativeCommutativeExpr::new(pair.0, pair.1))
    }
}

fn build_multiplication_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::RationalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs *= rhs;
            Expr::RationalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::DecimalExpr(rhs)) => {
            lhs *= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs *= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::RationalExpr(lhs), Expr::DecimalExpr(mut rhs)) => {
            rhs *= lhs;
            Expr::DecimalExpr(rhs)
        },
        (Expr::MultiplicationExpr(lhs), Expr::MultiplicationExpr(rhs)) => {
            Expr::MultiplicationExpr(AssociativeCommutativeExpr::merge(rhs, lhs))
        },
        (Expr::MultiplicationExpr(mut lhs), rhs) => {
            lhs.append_right(rhs);
            Expr::MultiplicationExpr(lhs)
        },
        (lhs, Expr::MultiplicationExpr(mut rhs)) => {
            rhs.append_left(lhs);
            Expr::MultiplicationExpr(rhs)
        },
        _ => Expr::MultiplicationExpr(AssociativeCommutativeExpr::new(pair.0, pair.1))
    }
}

fn build_subtraction_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::RationalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs -= rhs;
            Expr::RationalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::DecimalExpr(rhs)) => {
            lhs -= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs -= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::RationalExpr(lhs), Expr::DecimalExpr(mut rhs)) => {
            rhs -= lhs;
            rhs *= -1; //TODO: optimize
            Expr::DecimalExpr(rhs)
        },
        (lhs, rhs) => Expr::SubtractionExpr(Box::new(lhs), Box::new(rhs))
    }
}

fn build_division_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::RationalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs /= rhs;
            Expr::RationalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::DecimalExpr(rhs)) => {
            lhs /= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::DecimalExpr(mut lhs), Expr::RationalExpr(rhs)) => {
            lhs /= rhs;
            Expr::DecimalExpr(lhs)
        },
        (Expr::RationalExpr(lhs), Expr::DecimalExpr(mut rhs)) => {
            rhs /= lhs;
            Expr::DecimalExpr(rhs.recip())
        },
        (lhs, rhs) => Expr::DivisionExpr(Box::new(lhs), Box::new(rhs))
    }
}

fn build_maximum_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::BooleanExpr(lhs), Expr::BooleanExpr(rhs)) => Expr::BooleanExpr(lhs && rhs),
        _ =>
            panic!("Maximum (conjunction) not yet implemented!Context:\n Pair: {:#?}\n", pair)
    }
}

fn build_minimum_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::BooleanExpr(lhs), Expr::BooleanExpr(rhs)) => Expr::BooleanExpr(lhs || rhs),
        _ =>
            panic!("Minimum (disjunction) not yet implemented!Context:\n Pair: {:#?}\n", pair)
    }
}

fn build_equality_ast(lhs : Expr, rhs : Expr) -> Expr {
    let pair = (lhs, rhs);
    match pair {
        (Expr::RationalExpr(lhs), Expr::DecimalExpr(rhs)) => Expr::BooleanExpr(lhs == rhs),
        (Expr::DecimalExpr(lhs), Expr::RationalExpr(rhs)) => Expr::BooleanExpr(lhs == rhs),
        (lhs, rhs) => Expr::BooleanExpr(lhs == rhs)
    }
}

fn build_binary_ast(lhs : Expr, op : Pair<Rule>, rhs : Expr) -> Expr {
    match op.as_rule() {
        Rule::plus => build_addition_ast(lhs, rhs),
        Rule::minus => build_subtraction_ast(lhs, rhs),
        Rule::times => build_multiplication_ast(lhs, rhs),
        Rule::division => build_division_ast(lhs, rhs),
        Rule::conjunction => build_maximum_ast(lhs, rhs),
        Rule::disjunction => build_minimum_ast(lhs, rhs),
        Rule::equals => build_equality_ast(lhs, rhs),
        _ => panic!(
            "Operation {:?} not yet implemented!\n\nContext:\n L:{:#?} O:{:#?} R:{:#?}\n",
            op.as_rule(), lhs, op, rhs)
    }
}
