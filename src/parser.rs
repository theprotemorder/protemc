
#[derive(Parser)]
#[grammar = "protem.pest"]
pub struct ProTemParser;

use pest::prec_climber::PrecClimber;
use pest::prec_climber::Operator;

fn get_data_op_vector() -> Vec<Operator<Rule>> {
    use pest::prec_climber::Assoc::{Left, Right};
    // Follows the order of operations in aPToP (vs that of the ProTem specification), with some
    // modification, for example, logical not has higher priority than any binary operator save @
    vec![
        Operator::new(Rule::implication, Left)
            | Operator::new(Rule::reverse_implication, Left),
        Operator::new(Rule::disjunction, Left),
        Operator::new(Rule::conjunction, Left),
        Operator::new(Rule::equals, Left)
            | Operator::new(Rule::neq, Left)
            | Operator::new(Rule::lt, Left)
            | Operator::new(Rule::gt, Left)
            | Operator::new(Rule::leq, Left)
            | Operator::new(Rule::geq, Left)
            | Operator::new(Rule::bunch_inclusion, Left)
            | Operator::new(Rule::set_element, Left)
            | Operator::new(Rule::subset, Left),
        Operator::new(Rule::bunch_union, Left)
            | Operator::new(Rule::bunch_from, Left)
            | Operator::new(Rule::selective_union, Left)
            | Operator::new(Rule::string_modification, Left),
        Operator::new(Rule::string_catenation, Left)
            | Operator::new(Rule::string_from, Left)
            | Operator::new(Rule::bunch_intersection, Left),
        Operator::new(Rule::plus, Left)
            | Operator::new(Rule::minus, Right)
            | Operator::new(Rule::list_catenation, Left)
            | Operator::new(Rule::union, Left),
        Operator::new(Rule::times, Left)
            | Operator::new(Rule::division, Left)
            | Operator::new(Rule::intersection, Left),
        Operator::new(Rule::function_space, Right)
    ]
}

fn get_program_op_vector() -> Vec<Operator<Rule>> {
    use pest::prec_climber::Assoc::Left;
    vec![
        Operator::new(Rule::independent_composition, Left),
        Operator::new(Rule::sequential_composition, Left),
        Operator::new(Rule::assignment, Left)
            | Operator::new(Rule::channel_output, Left),
    ]
}

pub fn get_data_precedence_climber() -> PrecClimber<Rule> {
    return PrecClimber::new(get_data_op_vector());
}

pub fn get_program_precedence_climber() -> PrecClimber<Rule> {
    return PrecClimber::new(get_program_op_vector());
}

lazy_static! {
    pub static ref DATA_PREC_CLIMBER : PrecClimber<Rule> = get_data_precedence_climber();
    pub static ref PROGRAM_PREC_CLIMBER : PrecClimber<Rule> = get_program_precedence_climber();
}

#[cfg(test)]
mod test {
    use pest::iterators::Pairs;
    use pest::iterators::Pair;
    use pest::prec_climber::PrecClimber;
    use pest::Parser;
    use super::*;

    #[test]
    fn simple_names_are_parsed_properly() {
        let good_names = ["Test123", "<< w o a h >>", "<<<> mI + x > > »", "ifi"];
        for name in good_names.iter() {
            let pair = ProTemParser::parse(Rule::simple_name, name).unwrap().next().unwrap();
            assert_eq!(&pair.as_str(), name);
        }
        let half_names = ["Test 123", "good << bad > >"];
        for name in half_names.iter() {
            let pair = ProTemParser::parse(Rule::simple_name, name).unwrap().next().unwrap();
            assert_ne!(&pair.as_str(), name);
        }
        let bad_names = ["123Test", "<<bad> > good", "if", "for"];
        for name in bad_names.iter() {
            match ProTemParser::parse(Rule::simple_name, name) {
                Ok(_) => panic!("These things shouldn't parse!"),
                _ => {}
            }
        }
    }

    #[test]
    fn names_are_parsed_properly() {
        parses_to! {
            parser : ProTemParser,
            input : "test_best_rest_<< w o a h >>_<<<> mI + x > > »_ifi_<<for>>",
            rule : Rule::name,
            tokens : [
                name(0, 59, [
                    plain_simple_name(0, 4),
                    plain_simple_name(5, 9),
                    plain_simple_name(10, 14),
                    fancy_simple_name(15, 28, [
                        open_fancy(15, 17),
                        inner_name(17, 26),
                        close_fancy(26, 28)
                        ]),
                    fancy_simple_name(29, 47, [
                        open_fancy(29, 31),
                        inner_name(31, 45),
                        close_fancy(45, 47)
                        ]),
                    plain_simple_name(48, 51),
                    fancy_simple_name(52, 59, [
                        open_fancy(52, 54),
                        inner_name(54, 57),
                        close_fancy(57, 59)
                        ])
                    ])
            ]
        }
    }

    #[test]
    fn numbers_are_parsed_properly() {
        let good_numbers = ["123", "123.456", "100%", "200%", "10.353%"];
        for number in good_numbers.iter() {
            let pair = ProTemParser::parse(Rule::number, number).unwrap().next().unwrap();
            assert_eq!(&pair.as_str(), number)
        }
        let half_numbers = ["10.", "105.%"];
        for number in half_numbers.iter() {
            let pair = ProTemParser::parse(Rule::number, number).unwrap().next().unwrap();
            assert_ne!(&pair.as_str(), number);
        }
        let bad_numbers = [".123", ".53%"];
        for number in bad_numbers.iter() {
            match ProTemParser::parse(Rule::number, number) {
                Ok(_) => panic!("These things shouldn't parse!"),
                _ => {}
            }
        }
    }

    #[test]
    fn integer_percentages_are_parsed_properly() {
        parses_to!(
            parser : ProTemParser,
            input : "55%",
            rule : Rule::number,
            tokens : [percentage(0, 3, [
                    integer(0, 2),
                    percent_sign(2, 3)
                ])
            ]
        )
    }

    #[test]
    fn texts_are_parsed_properly() {
        let simple_text = "\"This is a simple text\"";
        let pair = ProTemParser::parse(Rule::text, simple_text).unwrap().next().unwrap();
        assert_eq!(pair.as_str(), simple_text);
    }

    #[test]
    fn expressions_are_parsed_properly() {
        parses_to! {
            parser : ProTemParser,
            input: "1 - 2 + name & number / true + (false * << w o w»)",
            rule: Rule::data_expression,
            tokens: [data_expression(0, 51, [
                unary_expression(0, 2, [integer(0, 1)]),
                minus(2, 3),
                unary_expression(4, 6, [integer(4, 5)]),
                plus(6, 7),
                unary_expression(8, 13, [name(8, 12, [plain_simple_name(8, 12)])]),
                times(13, 14),
                unary_expression(15, 22, [name(15, 21, [plain_simple_name(15, 21)])]),
                division(22, 23),
                unary_expression(24, 29, [name(24, 28, [plain_simple_name(24, 28)])]),
                plus(29, 30),
                unary_expression(31, 51, [expression(32, 50, [data_expression(32, 50, [
                    unary_expression(32, 38, [name(32, 37, [plain_simple_name(32, 37)])]),
                    definite_repetition(38, 39),
                    unary_expression(40, 50, [name(40, 50, [fancy_simple_name(40, 50, [
                        open_fancy(40, 42),
                        inner_name(42, 48),
                        close_fancy(48, 50)
                        ])])])
                    ])])])
                ])]
        };
        parses_to! {
            parser : ProTemParser,
            input : "x >= 5",
            rule: Rule::data_expression,
            tokens : [
                data_expression(0, 6, [
                    unary_expression(0, 2, [name(0, 1, [plain_simple_name(0, 1)])]),
                    geq(2, 4),
                    unary_expression(5, 6, [integer(5, 6)])
                    ])
            ]
        }
    }

    #[test]
    fn quantifiers_are_parsed_properly() {
        parses_to! {
            parser : ProTemParser,
            input : "P \\/ \\forall x : D \\cdot x >= 5",
            rule : Rule::data_expression,
            tokens : [data_expression(0, 31, [
                unary_expression(0, 2, [name(0, 1, [plain_simple_name(0, 1)])]),
                disjunction(2, 4),
                dot_quantifier_expression(5, 31, [
                    forall(5, 12),
                    data_expression(13, 19, [
                        unary_expression(13, 15, [name(13, 14, [plain_simple_name(13, 14)])]),
                        bunch_inclusion(15, 16),
                        unary_expression(17, 19, [name(17, 18, [plain_simple_name(17, 18)])])
                    ]),
                    cdot(19, 24),
                    expression(25, 31, [
                        data_expression(25, 31, [
                        unary_expression(25, 27, [name(25, 26, [plain_simple_name(25, 26)])]),
                        geq(27, 29),
                        unary_expression(30, 31, [integer(30, 31)])
                        ])]),
                    ])])]
        };
    }

    #[test]
    fn programs_are_parsed_correctly() {
        parses_to! {
            parser : ProTemParser,
            input : "new x : nat = 52 + 8. x := 5. x := x + 1",
            rule : Rule::expression,
            tokens : [
                expression(0, 40, [
                    variable_declaration(0, 20, [
                            new(0, 3),
                            plain_simple_name(4, 5),
                            bunch_inclusion(6, 7),
                            data_expression(8, 20, [
                                unary_expression(8, 12,
                                    [name(8, 11, [plain_simple_name(8, 11)])]),
                                equals(12, 13),
                                unary_expression(14, 17, [integer(14, 16)]),
                                plus(17, 18),
                                unary_expression(19, 20, [integer(19, 20)])
                            ])]),
                    sequential_composition(20, 21),
                    assign_variable(22, 28, [
                        plain_simple_name(22, 23),
                        assignment(24, 26),
                        data_expression(27, 28, [unary_expression(27, 28, [integer(27, 28)])])
                        ]),
                    sequential_composition(28, 29),
                    assign_variable(30, 40, [
                        plain_simple_name(30, 31),
                        assignment(32, 34),
                        data_expression(35, 40, [
                            unary_expression(35, 37, [name(35, 36, [plain_simple_name(35, 36)])]),
                            plus(37, 38),
                            unary_expression(39, 40, [integer(39, 40)])
                            ])
                        ])
                ])
            ]
        }
    }

    fn bracketize(climber: &PrecClimber<Rule>, expr: Pairs<Rule>) -> String {
        climber.climb(
            expr,
            |pair : Pair<Rule>| match pair.as_rule() {
                Rule::data_expression|Rule::expression => bracketize(climber, pair.into_inner()),
                _ => pair.as_str().to_owned()
            },
            |lhs: String, op: Pair<Rule>, rhs: String| {
                "(".to_owned() + &lhs.trim() + ") " + op.as_str() + " (" + &rhs.trim() + ")"
            }
        )
    }

    #[test]
    fn data_precedence_climber_works() {
        let expr = "3 + 5 & 7 - - 5 + 2";
        let desired = "(3) + ((((5) & (7)) - (- 5)) + (2))";

        let pairs = ProTemParser::parse(Rule::expression, expr).unwrap();
        assert_eq!(pairs.as_str(), expr);
        assert_eq!(bracketize(&DATA_PREC_CLIMBER, pairs), desired);
    }
}
