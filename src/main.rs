// Import macros
#[cfg_attr(test, macro_use)]
extern crate pest;
use pest::Parser;

#[macro_use]
extern crate pest_derive;

#[macro_use]
extern crate lazy_static;

// Readline. Code inspired by example on https://github.com/kkawakam/rustyline
extern crate rustyline;
use rustyline::error::ReadlineError;
use rustyline::Editor;

pub mod parser;
pub mod ast;

use self::parser::ProTemParser;
use self::parser::Rule;
use self::ast::build_ast;

fn main() {
    let mut rl = Editor::<()>::new();
    let mut buffer = String::new();

    println!("Welcome to the pseudo-ProTem REPL, v0.0");
    if rl.load_history("history.txt").is_err() {
        println!("No previous history loaded");
    }
    loop {
        let line = if buffer.is_empty() {rl.readline(">> ")} else {rl.readline("")};
        match line {
            Ok(line) => buffer += line.as_str(),
            Err(ReadlineError::Interrupted)|Err(ReadlineError::Eof) => break,
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
        match ProTemParser::parse(Rule::expression, buffer.as_str()) {
            Ok(mut pairs) => {
                match pairs.next() {
                    Some(pair) => {
                        let ast = build_ast(pair);
                        println!("AST: {:#?}", ast);
                        buffer.clear();
                    },
                    None => continue
                }
            },
            Err(_) => continue
        }
    }
    rl.save_history("history.txt").unwrap();
}
