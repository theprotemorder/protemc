An implementation of a modified, limited subset of [ProTem](http://cs.toronto.edu/~hehner/PT.pdf) based off the variant of the language described in [A Practical Theory of Programming](http://cs.toronto.edu/~hehner/PT.pdf) written in Rust.

Requires LLVM 7.0 and developed using Rust 1.31.0-nightly. 
